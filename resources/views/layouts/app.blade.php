<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laracasts Voting App</title>



    <!-- Fonts -->
    {{-- <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" /> --}}

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    @livewireStyles
</head>

<body class="bg-gray-background font-sans text-sm text-gray-900">
    <header class="flex flex-col items-center justify-between px-8 py-4 md:flex-row">
        <a href="/"><img class="h-16" src="{{ asset('./laracasts-logo.svg') }}" alt=""></a>
        <div class="mt-2 flex items-center md:mt-0">
            @if (Route::has('login'))
                <div class="px-6 py-4">
                    @auth
                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <a href="route('logout')" onclick="event.preventDefault();
                                        this.closest('form').submit();">
                                {{ __('Log Out') }}
                            </a>
                        </form>
                    @else
                        <a href="{{ route('login') }}" class="focus:outline-red-500 font-semibold text-gray-600 hover:text-gray-900 focus:rounded-sm focus:outline focus:outline-2 dark:text-gray-400 dark:hover:text-white">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="focus:outline-red-500 ml-4 font-semibold text-gray-600 hover:text-gray-900 focus:rounded-sm focus:outline focus:outline-2 dark:text-gray-400 dark:hover:text-white">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <a href="#">
                <img src="https://www.gravatar.com/avatar/0000000000000000000000000?d=mp" alt="avatar" class="h-10 w-10 rounded-full">
            </a>
        </div>

    </header>

    <main class="container mx-auto flex max-w-custom flex-col md:flex-row">
        <div class="mx-auto w-70 md:mx-0 md:mr-5">
            <div class="mt-16 rounded-xl border-2 border-blue bg-white"
                style="
                  border-image-source: linear-gradient(to bottom, rgba(50, 138, 241, 0.22), rgba(99, 123, 255, 0));
                    border-image-slice: 1;
                    background-image: linear-gradient(to bottom, #ffffff, #ffffff), linear-gradient(to bottom, rgba(50, 138, 241, 0.22), rgba(99, 123, 255, 0));
                    background-origin: border-box;
                    background-clip: content-box, border-box;
            ">
                <div class="px-6 py-2 pt-6 text-center">
                    <h3 class="text-base font-semibold">Add an idea</h3>
                    <p class="mt-4 text-xs">
                        @auth
                            Let us know what you would like and we'll take a look over!
                        @else
                            Please login to create an idea.
                        @endauth



                    </p>
                </div>

                @auth
                    <livewire:create-idea />
                @else
                    <div class="my-6 text-center">
                        <a href="{{ route('login') }}" class="inline-block h-11 w-1/2 justify-center rounded-xl border border-blue bg-blue px-6 py-3 text-xs font-semibold text-white transition duration-150 ease-in hover:bg-blue-hover">
                            Login
                        </a>

                        <a href="{{ route('register') }}" class="mt-4 inline-block h-11 w-1/2 justify-center rounded-xl border border-gray-200 bg-gray-200 px-6 py-3 text-xs font-semibold transition duration-150 ease-in hover:border-gray-400">
                            Sign Up
                        </a>



                    </div>

                @endauth




            </div>
        </div>
        <div class="w-full px-2 md:w-175 md:px-0">
            <livewire:status-filters />

            <div class="mt-8">
                {{ $slot }}
            </div>
        </div>

    </main>

    @livewireScripts

</body>

</html>
