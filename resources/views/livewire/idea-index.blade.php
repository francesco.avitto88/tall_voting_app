<div x-data @click="linkToCard($event)" class="idea-container hover:shadow-card flex cursor-pointer rounded-xl bg-white transition duration-150 ease-in">
    <div class="hidden border-r border-gray-100 px-5 py-8 md:block">
        <div class="text-center">
            <div class="@if ($hasVoted) text-blue @endif text-2xl font-semibold">{{ $votesCount }}</div>
            <div class="text-gray-500">Votes</div>
        </div>

        <div class="mt-8">
            @if ($hasVoted)
                <button wire:click.prevent="vote" class="text-xxs border-blue bg-blue hover:bg-blue-hover w-20 rounded-xl border px-4 py-3 font-bold uppercase text-white transition duration-150 ease-in">Voted</button>
            @else
                <button wire:click.prevent="vote" class="text-xxs w-20 rounded-xl border border-gray-200 bg-gray-200 px-4 py-3 font-bold uppercase transition duration-150 ease-in hover:border-gray-400">Vote</button>
            @endif
        </div>
    </div>
    <div class="flex flex-1 flex-col px-2 py-6 md:flex-row">
        <div class="mx-2 flex-none md:mx-0">
            <a href="#">
                <img src="{{ $idea->user->getAvatar() }}" alt="avatar" class="h-14 w-14 rounded-xl">
            </a>
        </div>
        <div class="mx-2 flex w-full flex-col justify-between md:mx-4">
            <h4 class="mt-2 text-xl font-semibold md:mt-0">
                <a href="{{ route('idea.show', $idea) }}" class="idea-link hover:underline">{{ $idea->title }}</a>
            </h4>
            <div class="line-clamp-3 mt-3 text-gray-600">
                {{ $idea->description }}
            </div>

            <div class="mt-6 flex flex-col justify-between md:flex-row md:items-center">
                <div class="flex items-center space-x-2 text-xs font-semibold text-gray-400">
                    <div>{{ $idea->created_at->diffForHumans() }}</div>
                    <div>&bull;</div>
                    <div>{{ $idea->category->name }}</div>
                    <div>&bull;</div>
                    <div class="text-gray-900">3 Comments</div>
                </div>
                <div x-data="{ isOpen: false }" class="mt-4 flex items-center space-x-2 md:mt-0">
                    <div class="{{ $idea->status->classes }} text-xxs h-7 w-28 rounded-full py-2 px-4 text-center font-bold uppercase leading-none">{{ $idea->status->name }}</div>
                  
                </div>

                <div class="mt-4 flex items-center md:mt-0 md:hidden">
                    <div class="h-10 rounded-xl bg-gray-100 px-4 py-2 pr-8 text-center">
                        <div class="@if ($hasVoted) text-blue @endif text-sm font-bold leading-none">{{ $votesCount }}</div>
                        <div class="text-xxs font-semibold leading-none text-gray-400">Votes</div>
                    </div>
                    @if ($hasVoted)
                        <button wire:click.prevent="vote" class="text-xxs border-blue bg-blue hover:bg-blue-hover -mx-5 w-20 rounded-xl border px-4 py-3 font-bold uppercase text-white transition duration-150 ease-in">
                            Voted
                        </button>
                    @else
                        <button wire:click.prevent="vote" class="text-xxs -mx-5 w-20 rounded-xl border border-gray-200 bg-gray-200 px-4 py-3 font-bold uppercase transition duration-150 ease-in hover:border-gray-400">
                            Vote
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div> <!-- end idea-container -->
