<div class="idea-and-buttons container">

    <div class="idea-container mt-4 flex rounded-xl bg-white">
        <div class="flex flex-1 flex-col px-4 py-6 md:flex-row">
            <div class="mx-2 flex-none">
                <a href="#">
                    <img src="{{ $idea->user->getAvatar() }}" alt="avatar" class="h-14 w-14 rounded-xl">
                </a>
            </div>
            <div class="mx-2 w-full md:mx-4">
                <h4 class="mt-2 text-xl font-semibold md:mt-0">
                    {{ $idea->title }}
                </h4>
                <div class="mt-3 text-gray-600">
                    {{ $idea->description }}
                </div>

                <div class="mt-6 flex flex-col justify-between md:flex-row md:items-center">
                    <div class="flex items-center space-x-2 text-xs font-semibold text-gray-400">
                        <div class="hidden font-bold text-gray-900 md:block">{{ $idea->user->name }}</div>
                        <div class="hidden md:block">&bull;</div>
                        <div>{{ $idea->created_at->diffForHumans() }}</div>
                        <div>&bull;</div>
                        <div>{{ $idea->category->name }}</div>
                        <div>&bull;</div>
                        <div class="text-gray-900">3 Comments</div>
                    </div>
                    <div class="mt-4 flex items-center space-x-2 md:mt-0" x-data="{ isOpen: false }">
                        <div class="{{ $idea->status->classes }} h-7 w-28 rounded-full px-4 py-2 text-center text-xxs font-bold uppercase leading-none">{{ $idea->status->name }}</div>
                        <div class="relative">
                            <button class="relative h-7 rounded-full border bg-gray-100 px-3 py-2 transition duration-150 ease-in hover:bg-gray-200" @click="isOpen = !isOpen">
                                <svg fill="currentColor" width="24" height="6">
                                    <path d="M2.97.061A2.969 2.969 0 000 3.031 2.968 2.968 0 002.97 6a2.97 2.97 0 100-5.94zm9.184 0a2.97 2.97 0 100 5.939 2.97 2.97 0 100-5.939zm8.877 0a2.97 2.97 0 10-.003 5.94A2.97 2.97 0 0021.03.06z" style="color: rgba(163, 163, 163, .5)">
                                </svg>
                            </button>
                            <ul class="absolute right-0 top-8 z-10 w-44 rounded-xl bg-white py-3 text-left font-semibold shadow-dialog md:left-0 md:top-6 md:ml-8" x-cloak x-show.transition.origin.top.left="isOpen" @click.away="isOpen = false" @keydown.escape.window="isOpen = false">
                                <li><a href="#" class="block px-5 py-3 transition duration-150 ease-in hover:bg-gray-100">Edit Idea</a></li>
                                <li><a href="#" class="block px-5 py-3 transition duration-150 ease-in hover:bg-gray-100">Delete Idea</a></li>

                                <li><a href="#" class="block px-5 py-3 transition duration-150 ease-in hover:bg-gray-100">Mark as Spam</a></li>
                            </ul>

                        </div>
                    </div>

                    <div class="mt-4 flex items-center md:mt-0 md:hidden">
                        <div class="h-10 rounded-xl bg-gray-100 px-4 py-2 pr-8 text-center">
                            <div class="@if ($hasVoted) text-blue @endif text-sm font-bold leading-none">{{ $votesCount }}</div>
                            <div class="text-xxs font-semibold leading-none text-gray-400">Votes</div>
                        </div>

                        @if ($hasVoted)
                            <button wire:click.prevent="vote" class="-mx-5 w-20 rounded-xl border border-blue bg-blue px-4 py-3 text-xxs font-bold uppercase text-white transition duration-150 ease-in hover:bg-blue-hover">
                                Voted
                            </button>
                        @else
                            <button wire:click.prevent="vote" class="-mx-5 w-20 rounded-xl border border-gray-200 bg-gray-200 px-4 py-3 text-xxs font-bold uppercase transition duration-150 ease-in hover:border-gray-400">
                                Vote
                            </button>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end idea-container -->

    <div class="buttons-container mt-6 flex items-center justify-between">
        <div class="flex flex-col items-center space-x-4 md:ml-6 md:flex-row">
            <div x-data="{ isOpen: false }" class="relative">
                <button type="button" @click="isOpen = !isOpen" class="flex h-11 w-32 items-center justify-center rounded-xl border border-blue bg-blue px-6 py-3 text-sm font-semibold text-white transition duration-150 ease-in hover:bg-blue-hover">
                    Reply
                </button>
                <div class="absolute z-10 mt-2 w-64 rounded-xl bg-white text-left text-sm font-semibold shadow-dialog md:w-104" x-cloak x-show.transition.origin.top.left="isOpen" @click.away="isOpen = false" @keydown.escape.window="isOpen = false">
                    <form action="#" class="space-y-4 px-4 py-6">
                        <div>
                            <textarea name="post_comment" id="post_comment" cols="30" rows="4" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm placeholder-gray-900" placeholder="Go ahead, don't be shy. Share your thoughts..."></textarea>
                        </div>

                        <div class="flex flex-col items-center md:flex-row md:space-x-3">
                            <button type="button" class="flex h-11 w-full items-center justify-center rounded-xl border border-blue bg-blue px-6 py-3 text-sm font-semibold text-white transition duration-150 ease-in hover:bg-blue-hover md:w-1/2">
                                Post Comment
                            </button>
                            <button type="button" class="mt-2 flex h-11 w-full items-center justify-center rounded-xl border border-gray-200 bg-gray-200 px-6 py-3 text-xs font-semibold transition duration-150 ease-in hover:border-gray-400 md:mt-0 md:w-32">
                                <svg class="w-4 -rotate-45 transform text-gray-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
                                </svg>
                                <span class="ml-1">Attach</span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>

            @if (auth()->check() &&
                    auth()->user()->isAdmin())
                <livewire:set-status :idea="$idea" />
            @endif
        </div>

        <div class="hidden items-center space-x-3 md:flex">
            <div class="rounded-xl bg-white px-3 py-2 text-center font-semibold">
                <div class="@if ($hasVoted) text-blue @endif text-xl leading-snug">{{ $votesCount }}</div>
                <div class="text-xs leading-none text-gray-400">Votes</div>
            </div>
            @if ($hasVoted)
                <button type="button" wire:click.prevent="vote" class="h-11 w-32 rounded-xl border border-blue bg-blue px-6 py-3 text-xs font-semibold uppercase text-white transition duration-150 ease-in hover:bg-blue-hover">
                    <span>Voted</span>
                </button>
            @else
                <button type="button" wire:click.prevent="vote" class="h-11 w-32 rounded-xl border border-gray-200 bg-gray-200 px-6 py-3 text-xs font-semibold uppercase transition duration-150 ease-in hover:border-gray-400">
                    <span>Vote</span>
                </button>
            @endif
        </div>
    </div> <!-- end buttons-container -->
</div> <!-- end ideas and buttons-container -->
