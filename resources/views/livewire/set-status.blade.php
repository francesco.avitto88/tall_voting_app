<div class="relative" x-data="{ isOpen: false }" x-init="window.livewire.on('statusWasUpdated', () => { isOpen = false })">
    <button type="button" @click="isOpen = !isOpen" class="mt-2 flex h-11 w-36 items-center justify-center rounded-xl border border-gray-200 bg-gray-200 px-6 py-3 text-sm font-semibold transition duration-150 ease-in hover:border-gray-400 md:mt-0">
        <span>Set Status</span>
        <svg class="ml-2 h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
        </svg>
    </button>
    <div x-cloak x-show.transition.origin.top.left="isOpen" @click.away="isOpen = false" @keydown.escape.window="isOpen = false" class="absolute z-20 mt-2 w-64 rounded-xl bg-white text-left text-sm font-semibold shadow-dialog md:w-76">
        <form wire:submit.prevent="setStatus" action="#" class="space-y-4 px-4 py-6">
            <div class="space-y-2">
                <div>
                    <label class="inline-flex items-center">
                        <input wire:model="status" type="radio" class="border-none bg-gray-200 text-gray-600" name="status" value="1" checked>
                        <span class="ml-2">Open</span>
                    </label>
                </div>
                <div>
                    <label class="inline-flex items-center">
                        <input wire:model="status" type="radio" class="border-none bg-gray-200 text-purple" name="status" value="2">
                        <span class="ml-2">Considering</span>
                    </label>
                </div>
                <div>
                    <label class="inline-flex items-center">
                        <input wire:model="status" type="radio" class="border-none bg-gray-200 text-yellow" name="status" value="3">
                        <span class="ml-2">In Progress</span>
                    </label>
                </div>
                <div>
                    <label class="inline-flex items-center">
                        <input wire:model="status" type="radio" class="border-none bg-gray-200 text-green" name="status" value="4">
                        <span class="ml-2">Implemented</span>
                    </label>
                </div>
                <div>
                    <label class="inline-flex items-center">
                        <input wire:model="status" type="radio" class="border-none bg-gray-200 text-red" name="status" value="5">
                        <span class="ml-2">Closed</span>
                    </label>
                </div>
            </div>

            <div>
                <textarea name="update_comment" id="update_comments" cols="30" rows="3" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm placeholder-gray-900" placeholder="Add an update comment (optional)"></textarea>
            </div>

            <div class="flex items-center justify-between space-x-3">
                <button type="button" class="flex h-11 w-1/2 items-center justify-center rounded-xl border border-gray-200 bg-gray-200 px-6 py-3 text-xs font-semibold transition duration-150 ease-in hover:border-gray-400">
                    <svg class="w-4 -rotate-45 transform text-gray-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
                    </svg>
                    <span class="ml-1">Attach</span>
                </button>
                <button type="submit" class="flex h-11 w-1/2 items-center justify-center rounded-xl border border-blue bg-blue px-6 py-3 text-xs font-semibold text-white transition duration-150 ease-in hover:bg-blue-hover disabled:opacity-50">
                    <span class="ml-1">Update</span>
                </button>
            </div>

            <div>
                <label class="inline-flex items-center font-normal">
                    <input wire:model="notifyAllVoters" type="checkbox" name="notify_voters" class="rounded bg-gray-200">
                    <span class="ml-2">Notify all voters</span>
                </label>
            </div>
        </form>
    </div>
</div>
