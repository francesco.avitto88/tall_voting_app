<div>
    <div class="filters flex flex-col space-y-3 md:flex-row md:space-x-6 md:space-y-0">
        <div class="w-full md:w-1/3">
            <select wire:model="category" name="category" id="category" class="w-full rounded-xl border-none px-4 py-2">
                <option value="All Categories">All Categories</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->name }}">{{ $category->name }}</option>
                @endforeach

            </select>
        </div>
        <div class="w-full md:w-1/3">
            <select wire:model="filter" name="other_filters" id="other_filters" class="w-full rounded-xl border-none px-4 py-2">
                <option value="No Filters">No Filters</option>
                <option value="Top Voted">Top Voted</option>
                <option value="My Ideas">My Ideas</option>
            </select>
        </div>
        <div class="relative w-full md:w-2/3">
            <input wire:model="search" type="search" placeholder="Find an idea" class="w-full rounded-xl border-none bg-white px-4 py-2 pl-8 placeholder-gray-900">
            <div class="itmes-center absolute top-0 ml-2 flex h-full">
                <svg class="w-4 text-gray-700" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                </svg>
            </div>
        </div>
    </div> <!-- end filters -->

    <div class="ideas-container my-8 space-y-6">
        @forelse ($ideas as $idea)
            <livewire:idea-index :idea="$idea" :key="$idea->id" :votesCount="$idea->votes_count" />
        @empty
            <div class="mx-auto mt-12 w-70">
                <img src="{{ asset('no-ideas.svg') }}" alt="No ideas were found.." class="mx-auto" style="mix-blend-mode:luminosity;">
                <div class="mt-6 text-center font-bold text-gray-400">No ideas were found..</div>
            </div>
        @endforelse

    </div> <!-- end ideas-container -->

    <div class="d-none bg-green bg-purple bg-red bg-yellow"></div>

    <div class="my-8">
        {{-- {{ $ideas->links() }} --}}
        {{ $ideas->appends(request()->query())->links() }}


    </div>

    <script>
        function linkToCard($event) {

            const clicked = $event.target
            const target = clicked.tagName.toLowerCase()
            const ignores = ['button', 'svg', 'path', 'a']
            if (!ignores.includes(target)) {
                clicked.closest('.idea-container').querySelector('.idea-link').click()
            }

        }
    </script>

</div>
