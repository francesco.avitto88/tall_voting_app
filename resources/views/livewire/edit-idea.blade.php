<div class="relative z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">

  <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

  <div class="fixed inset-0 z-10 w-screen overflow-y-auto">
    <div class="flex min-h-full items-center justify-center p-4 text-center sm:items-center sm:p-0">
      <!--
        Modal panel, show/hide based on modal state.

        Entering: "ease-out duration-300"
          From: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          To: "opacity-100 translate-y-0 sm:scale-100"
        Leaving: "ease-in duration-200"
            From: "opacity-100 translate-y-0 sm:scale-100"
          To: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
      -->
      <div class="relative transform overflow-hidden rounded-xl bg-white transition-all sm:my-8 sm:w-full sm:max-w-lg">
        <div class="absolute top-0 right-0 pt-4 pr-4">
            <button class="text-gray-400 hover:text-gray-500">
            <svg  fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
</svg>
</button></div>
        <div class="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
            <h3 class="text-center text-lg font-medium text-gray-800">
                Edit Idea
            </h3>
            <p class="text-xs text-center text-gray-500 mt-4 leading-4 px-4">You have one hour to edit your idea from the time you created it.</p>
        <form wire:submit.prevent="createIdea" action="#" method="POST" class="space-y-4 px-4 py-6">
    <div>
        <input wire:model.defer="title" type="text" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm placeholder-gray-900" placeholder="Your Idea" required>
        @error('title')
            <p class="text-red mt-1 text-sm">{{ $message }}</p>
        @enderror
    </div>
    <div>
        <select wire:model.defer="category" name="category_add" id="category_add" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm">
                <option value="1">Category 1 </option>

        </select>
    </div>
    @error('category')
        <p class="text-red mt-1 text-sm">{{ $message }}</p>
    @enderror
    <div>
        <textarea wire:model.defer="description" name="idea" id="idea" cols="30" rows="4" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm placeholder-gray-900" placeholder="Describe your idea" required></textarea>
        @error('description')
            <p class="text-red mt-1 text-sm">{{ $message }}</p>
        @enderror
    </div>
    <div class="flex items-center justify-between space-x-3">
        <button type="button" class="flex h-11 w-1/2 items-center justify-center rounded-xl border border-gray-200 bg-gray-200 px-6 py-3 text-xs font-semibold transition duration-150 ease-in hover:border-gray-400">
            <svg class="w-4 -rotate-45 transform text-gray-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
            </svg>
            <span class="ml-1">Attach</span>
        </button>
        <button type="submit" class="bg-blue border-blue hover:bg-blue-hover flex h-11 w-1/2 items-center justify-center rounded-xl border px-6 py-3 text-xs font-semibold text-white transition duration-150 ease-in">
            <span class="ml-1">Submit</span>
        </button>
    </div>
   
</form>
        </div>
        
      </div>
    </div>
  </div>
</div>
