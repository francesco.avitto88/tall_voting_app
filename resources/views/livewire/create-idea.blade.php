<form wire:submit.prevent="createIdea" action="#" method="POST" class="space-y-4 px-4 py-6">
    <div>
        <input wire:model.defer="title" type="text" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm placeholder-gray-900" placeholder="Your Idea" required>
        @error('title')
            <p class="text-red mt-1 text-sm">{{ $message }}</p>
        @enderror
    </div>
    <div>
        <select wire:model.defer="category" name="category_add" id="category_add" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm">
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach

        </select>
    </div>
    @error('category')
        <p class="text-red mt-1 text-sm">{{ $message }}</p>
    @enderror
    <div>
        <textarea wire:model.defer="description" name="idea" id="idea" cols="30" rows="4" class="w-full rounded-xl border-none bg-gray-100 px-4 py-2 text-sm placeholder-gray-900" placeholder="Describe your idea" required></textarea>
        @error('description')
            <p class="text-red mt-1 text-sm">{{ $message }}</p>
        @enderror
    </div>
    <div class="flex items-center justify-between space-x-3">
        <button type="button" class="flex h-11 w-1/2 items-center justify-center rounded-xl border border-gray-200 bg-gray-200 px-6 py-3 text-xs font-semibold transition duration-150 ease-in hover:border-gray-400">
            <svg class="w-4 -rotate-45 transform text-gray-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
            </svg>
            <span class="ml-1">Attach</span>
        </button>
        <button type="submit" class="bg-blue border-blue hover:bg-blue-hover flex h-11 w-1/2 items-center justify-center rounded-xl border px-6 py-3 text-xs font-semibold text-white transition duration-150 ease-in">
            <span class="ml-1">Submit</span>
        </button>
    </div>
    <div>
        @if (session('success_message'))
            <div x-data="{ isVisible: true }" x-init="setTimeout(() => {
                isVisible = false
            }, 5000)" x-show.transition.duration.1000ms="isVisible" class="text-green mt-4">
                {{ session('success_message') }}
            </div>
        @endif

    </div>
</form>
