<nav class="hidden items-center justify-between text-xs text-gray-400 md:flex">
    <ul class="flex space-x-10 border-b-4 pb-3 font-semibold uppercase">
        <li><a wire:click.prevent="setStatus('All')" class="@if ($status === 'All') text-gray-900 border-blue @endif border-b-4 pb-3 transition duration-150 ease-in hover:border-blue" href="{{ route('idea.index', ['status' => 'All']) }}">All Ideas ({{ $statusCount['all_statuses'] }})</a></li>
        <li><a wire:click.prevent="setStatus('Considering')" class="@if ($status === 'Considering') text-gray-900 border-blue @endif border-b-4 pb-3 transition duration-150 ease-in hover:border-blue" href="{{ route('idea.index', ['status' => 'Considering']) }}">Considering ({{ $statusCount['considering'] }})</a></li>
        <li><a wire:click.prevent="setStatus('In Progress')" class="@if ($status === 'In Progress') text-gray-900 border-blue @endif border-b-4 pb-3 transition duration-150 ease-in hover:border-blue" href="{{ route('idea.index', ['status' => 'In Progress']) }}">In progress ({{ $statusCount['in_progress'] }})</ text-gray-400a>
        </li>



    </ul>

    <ul class="flex space-x-10 border-b-4 pb-3 font-semibold uppercase">
        <li><a wire:click.prevent="setStatus('Implemented')" class="@if ($status === 'Implemented') text-gray-900 border-blue @endif border-b-4 pb-3 transition duration-150 ease-in hover:border-blue" href="{{ route('idea.index', ['status' => 'Implemented']) }}">Implemented ({{ $statusCount['implemented'] }})</a></li>
        <li><a wire:click.prevent="setStatus('Closed')" class="@if ($status === 'Closed') text-gray-900 border-blue @endif border-b-4 pb-3 transition duration-150 ease-in hover:border-blue" href="{{ route('idea.index', ['status' => 'Closed']) }}">Closed ({{ $statusCount['closed'] }})</a></li>

    </ul>
</nav>
